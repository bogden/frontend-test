$(function () {

    // ------------ Открытие закрытие модалки ------------ //

    window.$$ = {};
    $$.modal = function () {
        var $modal = $('[data-change-avatar-modal]');
        $(document)
            .on('modal.close', function (e) {
                e.preventDefault();
                $modal.addClass('deactive').removeClass('active');
                setTimeout(function () {
                    $modal.removeClass('deactive')
                },1500);
            })
            .on('modal.open', function (e) {
                e.preventDefault();
                $modal.addClass('active');
            });
        $('[data-close-modal]').on('click', function (e) {
            e.preventDefault();
            $modal.trigger('modal.close')
        });
        $('[data-open-modal]').on('click', function (e) {
            e.preventDefault();
            $modal.trigger('modal.open')
        });
    };

    // ------------ Загрузка и сохранение ------------ //

    $$.uploadAvatar = function () {
        var $steps = $('[data-upload-avatar]');
        $steps.hide();
        $steps.filter('[data-upload-avatar="1"]').show();
        $('[data-file-drop]').fileDrop();
        $(document)
            .on('uploaded.avatar-upload', function (e, data) {
                $steps.filter('[data-upload-avatar="1"]').hide();
                $steps.filter('[data-upload-avatar="2"]').show();
                if (data.src) {
                    var $image = $('<img data-cropper-image src="' + data.src + '">');
                    $('[data-cropper-image]').remove();
                    $('[data-cropper]').append($image);
                    $('[data-cropper-content]').drag();
                    $('[data-cropper-delete]').on('click', function () {
                        $(document).trigger('delete.avatar-upload');
                    });
                }
            })
            .on('delete.avatar-upload', function (e) {
                e.preventDefault();
                $steps.filter('[data-upload-avatar="2"]').hide();
                $steps.filter('[data-upload-avatar="1"]').show();
                // Тут Ajax для сервера на удаление
            })
            .on('success.avatar-upload', function (e, data) {
                e.preventDefault();
                $(document).trigger('modal.close');
                $steps.filter('[data-upload-avatar="2"]').hide();
                $steps.filter('[data-upload-avatar="1"]').show();
                var img = new Image(),
                    $avatar = $('#avatar'),
                    ctx = $avatar[0].getContext('2d');
                img.src = $(data.image).attr('src');
                img.onload = function () {
                    ctx.drawImage(img, data.sx, data.sy, data.swidth, data.sheight, data.x, data.y, 100, 100);
                };
            });
    };

    $$.init = function () {
        $$.modal();
        $$.uploadAvatar();
        return ['modal', 'uploadAvatar'];
    }();
});