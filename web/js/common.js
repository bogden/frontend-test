'use strict';

$(function () {
    // Плагин загрузки файла чере drag&drop и отправки файла через ajax
    jQuery.fn.fileDrop = function () {
        return this.each(function () {
            var $this = $(this),
                $input = $this.find('input[type="file"]'),
                $label = $this.find('label'),
                $progress = $this.find('[data-progress]'),
                $errorMsg = $this.find('[data-error-msg]');
            var droppedFiles = null;
            if (!$this.is('form')) {
                return false;
            }
            var maxFileSize = 1677721;
            var isAdvancedUpload = function () {
                var div = document.createElement('div');
                return ('draggable' in div || 'ondragstart' in div && 'ondrop' in div) && 'FormData' in window && 'FileReader' in window;
            }(),
                uploadProgress = function uploadProgress(event) {
                var percent = parseInt(event.loaded / event.total * 100);
                $progress.width(percent);
            },
                showFiles = function showFiles(files) {
                if (files !== undefined) {
                    return files.length <= 1;
                } else return false;
            };
            if (!isAdvancedUpload) {
                $label.html('<span class="link">Выберите файл на компьютере</span>');
                $this.find('[data-messages]').addClass('error');
            } else {
                $(document).on('drop', function (e) {
                    e.preventDefault();
                });
                $this.addClass('has-advanced-upload').on('drag dragstart dragend dragover dragenter dragleave drop', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                }).on('dragover dragenter', function () {
                    $this.addClass('is-dragover');
                }).on('dragleave dragend drop', function () {
                    $this.removeClass('is-dragover');
                }).on('drop change', function (e) {
                    droppedFiles = e.target.files || e.originalEvent.dataTransfer.files;
                    var imageFile = droppedFiles[0].type;
                    var match = ["image/jpeg", "image/png", "image/jpg"];
                    $this.removeClass('is-error');
                    $errorMsg.text(' ');
                    if (!(imageFile === match[0] || imageFile === match[1] || imageFile === match[2])) {
                        $this.addClass('is-error');
                        $errorMsg.text('Выберите изобращение (JPEG или PNG)');
                        $this.removeClass('drop').addClass('error');
                        return false;
                    } else if (droppedFiles[0].size > maxFileSize) {
                        $this.addClass('is-error');
                        $errorMsg.text('Слишком большой файл');
                        $this.removeClass('drop').addClass('error');
                        return false;
                    } else if (!showFiles(droppedFiles)) {
                        $this.addClass('is-error');
                        $errorMsg.text('Drop one image file!');
                        setTimeout(function () {
                            $this.removeClass('is-error');
                            $errorMsg.text(' ');
                        }, 3000);
                    } else {
                        $this.trigger('submit');
                        $input.replaceWith($input.val('').clone(true));
                    }
                });
            }
            $this.on('submit', function (e) {
                if ($this.hasClass('is-uploading')) return false;
                $this.addClass('is-uploading').removeClass('is-error');
                if (isAdvancedUpload) {
                    e.preventDefault();
                    var ajaxData = new FormData($this.get(0));
                    if (droppedFiles) {
                        $.each(droppedFiles, function (i, file) {
                            ajaxData.append($input.attr('name'), file);
                        });
                    }

                    $.ajax({
                        url: $this.attr('action'),
                        type: $this.attr('method'),
                        data: ajaxData,
                        dataType: 'json',
                        cache: false,
                        contentType: false,
                        processData: false,
                        xhr: function xhr() {
                            var myXhr = $.ajaxSettings.xhr();
                            if (myXhr.upload) {
                                myXhr.upload.addEventListener('progress', uploadProgress, false);
                            }
                            return myXhr;
                        },
                        complete: function complete() {
                            $this.removeClass('is-uploading');
                        },
                        success: function success(data) {
                            if (data.success) {
                                $this.addClass('is-success');
                                $this.trigger('uploaded.avatar-upload', data);
                            } else {
                                $this.addClass('is-error');
                                $errorMsg.text(data.error);
                            }
                        },
                        error: function error(e) {
                            console.log(e);
                            $label.html('Error');
                        }
                    });
                } else {
                    var iframeName = 'uploadiframe' + new Date().getTime();
                    var $iframe = $('<iframe name="' + iframeName + '" style="display: none;"></iframe>');
                    $('body').append($iframe);
                    $this.attr('target', iframeName);
                    $iframe.one('load', function () {
                        var data = JSON.parse($iframe.contents().find('body').text());
                        $this.removeClass('is-uploading').addClass(data.success === true ? 'is-success' : 'is-error').removeAttr('target');
                        if (!data.success) $errorMsg.text(data.error);
                        $this.removeAttr('target');
                        $iframe.remove();
                    });
                }
            });
            $input.on('change', function (e) {
                if (showFiles(e.target.files)) {}
            });
        });
    };
    // Плагин для кропа и ресайза картинки
    jQuery.fn.drag = function (opt) {
        opt = $.extend({ cursor: "move" }, opt);
        var $this = $(this),
            $parent = $this.parent(),
            $image = $parent.find('[data-cropper-image]'),
            $previews = $('[data-cropper-preview]'),
            $target = null,
            drag = false,
            resize = false,
            z_idx = $this.css('z-index'),
            mouseX = 0,
            mouseY = 0,
            drg_h = $this.outerHeight(),
            drg_w = $this.outerWidth(),
            pos_y = $this.offset().top + drg_h,
            pos_x = $this.offset().left + drg_w,
            top = parseFloat($this.css('top')),
            left = parseFloat($this.css('left')),
            parent_size = $parent.outerWidth(),
            offset = 20,
            positionTop = parseInt($this.position().top),
            positionLeft = parseInt($this.position().left),
            positionRight = parseInt($this.position().left + drg_w),
            positionBottom = parseInt($this.position().top + drg_h),
            relativePositionTop = parseInt(positionTop / 300 * 100),
            relativePositionLeft = parseInt(positionLeft / 300 * 100),
            relativePositionRight = parseInt((1 - positionRight / 300) * 100),
            relativePositionBottom = parseInt((1 - positionBottom / 300) * 100),
            zoomSize = 100 - (relativePositionLeft + relativePositionRight);

        //reset
        $this.css({
            width: '90%',
            height: '90%',
            top: '5%',
            left: '5%'
        });

        $('[data-cropper-save]').on('click', function (e) {
            e.preventDefault();
            $this.trigger('success.avatar-upload', {
                image: $image,
                sx: positionLeft,
                sy: positionTop,
                swidth: drg_w,
                sheight: drg_w,
                x: 0,
                y: 0,
                width: parseInt($this.outerWidth()),
                height: parseInt($this.outerHeight())
            });
        });

        var preview = function preview() {
            $previews.each(function () {
                var $this = $(this),
                    img = new Image(),
                    ctx = $this.find('canvas').eq(0)[0].getContext('2d');
                img.src = $image.attr('src');
                img.onload = function () {
                    ctx.drawImage(img, positionLeft, positionTop, drg_w, drg_w, 0, 0, parseInt($this.outerWidth()), parseInt($this.outerHeight()));
                };
            });
        };
        preview();

        $(document).on('mouseup touchend', function (e) {
            drag = false;
            resize = false;
            $target = null;
            $this.removeClass('draggable').removeClass('draggable');
            positionTop = parseInt($this.position().top);
            positionLeft = parseInt($this.position().left);
            positionRight = parseInt($this.position().left + drg_w);
            positionBottom = parseInt($this.position().top + drg_h);
            relativePositionTop = parseInt(positionTop / parent_size * 100);
            relativePositionLeft = parseInt(positionLeft / parent_size * 100);
            relativePositionRight = parseInt((1 - positionRight / parent_size) * 100);
            relativePositionBottom = parseInt((1 - positionBottom / parent_size) * 100);
            zoomSize = 100 - (relativePositionLeft + relativePositionRight);
            z_idx = $this.css('z-index');
            drg_h = $this.outerHeight();
            drg_w = $this.outerWidth();
            pos_y = $this.offset().top + drg_h - e.pageY;
            pos_x = $this.offset().left + drg_w - e.pageX;
            preview();

            if ($this.position().left > parent_size - drg_w) $this.css({ left: parent_size - drg_w });
            if ($this.position().top > parent_size - drg_h) $this.css({ top: parent_size - drg_w });
            if ($this.position().left < 0) $this.css({ left: '0px' });
            if ($this.position().top < 0) $this.css({ top: '0px' });
        });

        $(document).on("mousemove touchmove", function (e) {
            if (resize && $target) {
                if ($this.width() > 99) {
                    var coord, S;
                    if ($target.data('resize') === 'tl') {
                        coord = {
                            X: e.pageX - mouseX,
                            Y: e.pageY - mouseY
                        };
                        S = coord['X'] < coord['Y'] ? 'X' : 'Y';
                        $this.css({
                            width: drg_w - coord[S] + 'px',
                            height: drg_w - coord[S] + 'px',
                            top: top + coord[S] + 'px',
                            left: left + coord[S] + 'px'
                        });
                    } else if ($target.data('resize') === 'tr') {
                        coord = {
                            X: e.pageX - mouseX,
                            Y: e.pageY - mouseY
                        };
                        S = coord['X'] < coord['Y'] ? 'X' : 'Y';
                        $this.css({
                            width: drg_w + coord[S] + 'px',
                            height: drg_w + coord[S] + 'px',
                            top: top - coord[S] + 'px'
                        });
                    } else if ($target.data('resize') === 'bl') {
                        coord = {
                            X: e.pageX - mouseX,
                            Y: e.pageY - mouseY
                        };
                        S = coord['X'] < coord['Y'] ? 'X' : 'Y';
                        $this.css({
                            width: drg_w - -coord[S] + 'px',
                            height: drg_w - -coord[S] + 'px',
                            left: left - coord[S] + 'px'
                        });
                    } else {
                        coord = {
                            X: e.pageX - mouseX,
                            Y: e.pageY - mouseY
                        };
                        S = coord['X'] < coord['Y'] ? 'X' : 'Y';
                        $this.css({
                            width: drg_w + coord[S] + 'px',
                            height: drg_w + coord[S] + 'px'
                        });
                    }
                }
            }
        });

        return $this.css('cursor', opt.cursor).on("mousedown touchstart", function (e) {
            e.preventDefault();
            var $this = $(this);
            z_idx = $this.css('z-index');
            drg_h = $this.height();
            drg_w = $this.width();
            pos_y = $this.offset().top + drg_h - e.pageY;
            pos_x = $this.offset().left + drg_w - e.pageX;
            mouseX = parseInt(e.pageX);
            mouseY = parseInt(e.pageY);
            top = parseInt($this.css('top'));
            left = parseInt($this.css('left'));
            if (e.pageY > $this.offset().top + offset && e.pageY < $this.offset().top + drg_h - offset && e.pageX > $this.offset().left + offset && e.pageX < $this.offset().left + drg_w - offset) {
                drag = true;
            } else {
                $target = $(e.target);
                resize = true;
            }
            $this.addClass('draggable').css('z-index', 1000).parents();
        }).on("mousemove touchmove", function (e) {
            if (drag) {
                var n_top = e.pageY + pos_y - drg_h;
                var n_left = e.pageX + pos_x - drg_w;
                $this.offset({
                    top: $parent.offset().top >= n_top ? $parent.offset().top : $parent.offset().top + $parent.outerHeight() <= n_top + drg_h ? $parent.offset().top + $parent.outerHeight() - drg_h : n_top,
                    left: $parent.offset().left >= n_left ? $parent.offset().left : $parent.offset().left + $parent.outerHeight() <= n_left + drg_w ? $parent.offset().left + $parent.outerHeight() - drg_w : n_left
                });
            }
        });
    };
});
'use strict';

$(function () {

    // ------------ Открытие закрытие модалки ------------ //

    window.$$ = {};
    $$.modal = function () {
        var $modal = $('[data-change-avatar-modal]');
        $(document).on('modal.close', function (e) {
            e.preventDefault();
            $modal.addClass('deactive').removeClass('active');
            setTimeout(function () {
                $modal.removeClass('deactive');
            }, 1500);
        }).on('modal.open', function (e) {
            e.preventDefault();
            $modal.addClass('active');
        });
        $('[data-close-modal]').on('click', function (e) {
            e.preventDefault();
            $modal.trigger('modal.close');
        });
        $('[data-open-modal]').on('click', function (e) {
            e.preventDefault();
            $modal.trigger('modal.open');
        });
    };

    // ------------ Загрузка и сохранение ------------ //

    $$.uploadAvatar = function () {
        var $steps = $('[data-upload-avatar]');
        $steps.hide();
        $steps.filter('[data-upload-avatar="1"]').show();
        $('[data-file-drop]').fileDrop();
        $(document).on('uploaded.avatar-upload', function (e, data) {
            $steps.filter('[data-upload-avatar="1"]').hide();
            $steps.filter('[data-upload-avatar="2"]').show();
            if (data.src) {
                var $image = $('<img data-cropper-image src="' + data.src + '">');
                $('[data-cropper-image]').remove();
                $('[data-cropper]').append($image);
                $('[data-cropper-content]').drag();
                $('[data-cropper-delete]').on('click', function () {
                    $(document).trigger('delete.avatar-upload');
                });
            }
        }).on('delete.avatar-upload', function (e) {
            e.preventDefault();
            $steps.filter('[data-upload-avatar="2"]').hide();
            $steps.filter('[data-upload-avatar="1"]').show();
            // Тут Ajax для сервера на удаление
        }).on('success.avatar-upload', function (e, data) {
            e.preventDefault();
            $(document).trigger('modal.close');
            $steps.filter('[data-upload-avatar="2"]').hide();
            $steps.filter('[data-upload-avatar="1"]').show();
            var img = new Image(),
                $avatar = $('#avatar'),
                ctx = $avatar[0].getContext('2d');
            img.src = $(data.image).attr('src');
            img.onload = function () {
                ctx.drawImage(img, data.sx, data.sy, data.swidth, data.sheight, data.x, data.y, 100, 100);
            };
        });
    };

    $$.init = function () {
        $$.modal();
        $$.uploadAvatar();
        return ['modal', 'uploadAvatar'];
    }();
});
"use strict";
//# sourceMappingURL=common.js.map
